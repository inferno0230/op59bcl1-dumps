# sys_mssi_64_cn_armv82-user 13 TP1A.220905.001 1688736705992 release-keys
- manufacturer: oplusoplus
- platform: commonmt6983
- codename: ossi
- flavor: sys_mssi_64_cn_armv82-user
- release: 13
- id: TP1A.220905.001
- incremental: 1688736705992
- tags: release-keys
- fingerprint: oplus/ossi/ossi:12/SP1A.210812.016/1688651377686:user/release-keys
oplus/ossi/ossi:12/SP1A.210812.016/1688651377686:user/release-keys
- is_ab: true
- brand: oplus
- branch: sys_mssi_64_cn_armv82-user-13-TP1A.220905.001-1688736705992-release-keys
- repo: oplus_ossi_dump
